![TBZ Logo](./x_gitressourcen/tbz_logo.png)
![m319 Picto](./x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

### Modulidentifikation
[Link](https://gitlab.com/modulentwicklungzh/cluster-platform/m122)

--- 

## Einführung

Das Modul behandelt die Automatisierung von System-Abläufen mittels einer Skriptsprache. Diese muss von einem Interpreter (den sog. Scripting-**Host**) ausgeführt werden. 
Es gibt verschiedene [Skriptsprachen](https://de.wikipedia.org/wiki/Skriptsprache) in folgenden vier Anwendungsbereichen:

![](./x_gitressourcen/Anwendungsbereiche.png)

| Anwendungsbereich |  Skriptsprache (Interpreter = Scripting-"Host") z.Bsp.| Zweck der Automation (bzw. Fokus der Scriptsprache) |
| --- |:--- | --- |
| **Betriebssystem** |  **Bash (Unix, Linux, macOS)** <br> Batch (DOS, WINDOWS CMD) <br> Powershell (WINDOWS, Linux) <br> AppleScript (macOS) <br> Python| Scripts für Login, Installation, Konfiguration, usw. für das Betriebssystem oder dessen Nutzer(-accounts) |
| **Applikation** | VBA (MS Office, Libre Office) <br> LUA (Für eigene App)<br> AppleScript (mac-Apps) <br> UnityScript (Unity) | Ausführen von Abläufen innerhalb der Applikation |
| **WEB-Client** | JavaScript, TypeScript, JScript, Dart, ... (Interpreter im Browser) <br> JavaApplet (Java VM) | Die Scripts machen die Webseite dynamisch |
| **WEB-Server** |  PHP, NodeJS, Perl (Interpreter im Server) <br> JavaServlet (Java VM) <br> Python| Über die Scripts werden die Anfragen abgearbeitet. Die Logik der Web-Anwendungen wird damit aufgerufen oder ausgeführt. |

### Kommandozeileninterpreter

Manche Skriptsprachen sind von den Kommandozeileninterpretern der Betriebssysteme  abgeleitet. Die Interpreter sind vorrangig für interaktive Benutzung, d.h. für die Eingabe von Kommandos, ausgelegt. 

![](./x_gitressourcen/CompilerInterpreter.png)

*(Good to know: Bei einem Interpreter wird der Programmtext Zeile für Zeile abgearbeitet, wobei er das Programm auf Fehler überprüft. Jeder gefundene Fehler führt bei einem Interpreter zum Abbruch der Übersetzung; die fehlerhafte Zeile wird angezeigt und kann berichtigt werden. Anschließend kann der Übersetzungsprozess neu gestartet werden. Leider wird die Fehlerprüfung auch dann beibehalten, wenn das Programm fehlerfrei und lauffähig ist. Wieder wird der Programmtext Zeile für Zeile in Maschinencode übersetzt und ausgeführt. Da die Fehlerüberprüfung recht viel Zeit erfordert, sind durch einen Interpreter übersetzte Programme relativ langsam und dadurch für komplexe Lösungen untauglich >> Hochsprachen mit Compiler.)*

Die Eingabesprache wird um Variablen, arithmetische Ausdrücke, Kontrollstrukturen (if, while) und anderes erweitert und ermöglicht so die Automatisierung von Aufgaben (z. B. bei der unbeaufsichtigten Installation), indem „kleine Programme“ in Dateien geschrieben werden. Diese Dateien können dann vom Interpreter ausgeführt werden. Die Dateien nennt man unter dem Betriebssystem Unix/Linux **Shellskripte** (ausgeführt von einer der Unix-Shells bash, csh …) oder unter DOS und Windows auch Stapelverarbeitungsdateien oder Batch-Skripte (Ausgeführt von cmd.exe, powershell.exe).  (Wikipedia) 

In diesem Modul beginnen wir mit **Bash auf einem Linux-System** ...

---

##  Lektionenplan
| Tag  |  Thema | Bemerkung: Auftrag, Übungen |
|:----:|:------ |:-------------- |
|  19.05.22   |  [Linux Einführung](./01_Linux_Einf/README.md)  | Virtualisierung (oder Raspberry Pi) & Linux Distribution installieren <br> Erste Schritte mit Bash |
|  02.06.22  | [Bash Grundlagen](./02_Bash_Grundl/README.md) <br> [Bash Übungen](./03_Bash_Ueb/README.md) | Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen |
|  09.06.22   | [Bash Grundlagen](./02_Bash_Grundl/README.md) <br> [Bash Übungen --> Loesungen](./03_Bash_Ueb/Bash_Ueb_Loesungen.md) <br> [Bash Aufgaben](./04_Bash_Aufg/README.md) | Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen <br> Aufgaben 1 & 2|
|  16.06.22   | [Bash Aufgaben >> Lösungen](./04_Bash_Aufg/Bash_Aufg_Loes.md) <br> [Vorbereitung LB1 & Lösungen](./05_Bash_Vorb_LB1/README.md) <br> - <br> **LB1 (1 Lektion)** |  Repetition, Aufgaben fertig --> Lösungen                     |
|  23.06.22   | **LB2 Start: Projekt** <br> P-Design <br> P-Code | **Meilenstein A** Abgabe Projekt Design (23.06.22 12:00) |
|  30.06.22   | P-Code | |
|  07.07.22   | P-Code | |
|  14.07.22   | **Projektabgabe** <br> Demo, P-Doku, P-Test | **Meilenstein C** Projektdemo <br> **Meilenstein D** Abgabe: Code, Testbericht und Doku (14.07.22 00:01)                     |


## Projekt Rahmenbedingungen

* Da die Zeit sehr knapp ist gebe ich euch das Projekt vor. Ihr findet das Projekt [hier](10_Projekte_LB2/LB2_Projekt-gittools/Readme.md)
